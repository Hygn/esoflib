def solver(url,cookie,explain_=False):
    import pycurl
    import time
    import wget
    from urllib.parse import urlencode
    from io import BytesIO
    import platform
    global curl
    def curl(url, postfields, cookie, posten, os, browser):
        buffer = BytesIO()
        curl = pycurl.Curl()
        curl.setopt(curl.URL, url)
        if posten:
            curl.setopt(curl.POSTFIELDS, postfields)
        else:
            pass
        curl.setopt(pycurl.COOKIE, cookie)
        if os == "windows":
            if browser == "chrome":
                curl.setopt(pycurl.USERAGENT,
                   "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36")
            if browser== "firefox":
                curl.setopt(pycurl.USERAGENT,
                        "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:75.0) Gecko/20100101 Firefox/75.0")
        if os == "linux":
            if browser == "chrome":
                curl.setopt(pycurl.USERAGENT,
                       "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.122 Safari/537.36")
            if browser== "firefox":
                curl.setopt(pycurl.USERAGENT,
                        "Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:75.0) Gecko/20100101 Firefox/75.0")
        curl.setopt(pycurl.WRITEDATA, buffer)
        curl.setopt(pycurl.SSL_VERIFYPEER, 0)
        curl.setopt(pycurl.SSL_VERIFYHOST, 0)
        curl.perform()
        curl.close()
        dat = buffer.getvalue().decode('UTF-8')
        del buffer
        return dat
    def explain(quesnum,params,cnts,hoc,index_,cookie):
        if index_ != '*':
            quesnum = 1
        for i in range(int(quesnum)):
            if index_ != '*':
                i = index_
            post_data = {
            'stepSn': params[1].split("=")[1],
            'sessSn': '',
            'atnlcNo': params[0].split("=")[1],
            'lctreSn': params[2].split("=")[1],
            'cntntsTyCode': cnts}
            post_data.update({'pageIndex':str(i+1)})
            postfields = urlencode(post_data)
            dat = curl("https://"+hoc+".ebssw.kr/mypage/userlrn/userLrnQuizView.do",postfields, cookie, True, OS, browser)
            try:
                answerchk = 'https://'+hoc+'.ebssw.kr'+dat.split('input type="hidden" id="cnsrExplna" name="cnsrExplna" value="![')[1].split('](')[1].split(')')[0]
            except:
                answerchk = dat.split('input type="hidden" id="cnsrExplna" name="cnsrExplna" value="')[1].split('"')[0]
                if '&quot;' in answerchk:
                    answerchk = 'https://'+hoc+'.ebssw.kr'+answerchk.split('&quot;')[1].split('&quot;')[0]
            answer.append(answerchk)
        return answer
    global OS
    global browser
    if platform.system() == 'Linux':
        OS = "linux"
        browser = "firefox"
    else:
        OS = "windows"
        browser = "chrome"
    hoc = url.split("//")[1].split(".")[0]
    get = url.strip("https://"+hoc+".ebssw.kr/mypage/userlrn/userLrnView.do?")
    params = get.split("&")
    dat = curl(url, "", cookie, False , OS, browser)
    cnts = dat.split('loadCntnts( "'+params[2].split("=")[1]+'", "')[1].split('"')[0]
    quesnum = 1
    answer = []
    post_data = {
        'stepSn': params[1].split("=")[1],
        'sessSn': '',
        'atnlcNo': params[0].split("=")[1],
        'lctreSn': params[2].split("=")[1],
        'cntntsTyCode': cnts}
    postfields = urlencode(post_data)
    dat = curl("https://"+hoc+".ebssw.kr/mypage/userlrn/userLrnQuizView.do",postfields, cookie, True, OS, browser)
    quesnum = dat.split('span class="paging"><span class="current">1</span>')[1].split('<')[0].replace(' ', '').replace('/','').replace('\r','').replace('\t','')
    for q in range(int(quesnum)):
        post_data = {
            'stepSn': params[1].split("=")[1],
            'sessSn': '',
            'atnlcNo': params[0].split("=")[1],
            'lctreSn': params[2].split("=")[1],
            'cntntsTyCode': cnts}
        post_data.update({'pageIndex':str(q+1)})
        postfields = urlencode(post_data)
        dat = curl("https://"+hoc+".ebssw.kr/mypage/userlrn/userLrnQuizView.do",postfields, cookie, True, OS, browser)
        cntntsSn = dat.split('var cntntsSn = "')[1].split('"')[0]
        qestnSn = dat.split('"qestnSn" name="qestnSn" value="')[1].split('"')[0]
        cnsrCtrdCo = dat.split('name="cnsrCtrdCo" value="')[1].split('"')[0]
        cnts = dat.split('id="cntntsTyCode" name="cntntsTyCode" value="')[1].split('"')[0]
        rounds = dat.count('input type="radio"')
        try:
            exSns = dat.split('input type="radio" id="')[1].split('"')[0]
        except:
            return explain(quesnum,params,cnts,hoc,'*',cookie=cookie)
        questy = dat.split("javascript:chkAnswer( '")[1].split("'")[0]
        post_data = {
            'stepSn': params[1].split("=")[1],
            'atnlcNo': params[0].split("=")[1],
            'lctreSn': params[2].split("=")[1],
            'cntntsTyCode': cnts,
            'lctreSeCode': 'LCTRE',}
        postfields = urlencode(post_data)
        dat = curl("https://"+hoc+".ebssw.kr/mypage/userlrn/lctreLrnSave.do",postfields, cookie, True, OS, browser)
        post_data = {
            'lctreSn': params[2].split("=")[1],
            'cntntsUseTyCode': cnts}
        postfields = urlencode(post_data)
        dat = curl("https://"+hoc+".ebssw.kr/esof/cmmn/cntntsUseInsert.do",
            postfields, cookie, True, OS, browser)
        if explain_:
            return explain(quesnum,params,cnts,hoc,'*',cookie=cookie)

        for i in range(rounds):
            post_data = {
                'stepSn': params[1].split("=")[1],
                'atnlcNo': params[0].split("=")[1],
                'lctreSn': params[2].split("=")[1],
                'cntntsTyCode': cnts,
                'lctreSeCode': 'LCTRE',
                'qestnSn': str(int(qestnSn)),
                'cnsrCtrdCo': str(rounds),
                'quesTyCode': questy,
                'sessSn': '',
                'cntntsSn': cntntsSn}
            if rounds == 2:
                post_data.update({'exSn': str(int(exSns)+i), 'exNo': str(i+1)})
                if i == 0:
                    post_data.update({'exCn':'O'})
                else:
                    post_data.update({'exCn':'X'})
            else:
                post_data.update({'exSns': str(int(exSns)+i), 'exNos': str(i+1),})
            postfields = urlencode(post_data)
            print(postfields)
            dat = curl("https://"+hoc+".ebssw.kr/mypage/userlrn/chkQuizAnswerSelect.do",postfields, cookie, True, OS, browser)
            answerchk = dat.split(',"answerChk":"')[1].split('"')[0]
            print(dat)
            if answerchk == 'ANSWER':
                answer.append(i+1)
                post_data = {
                'lctreSn': params[2].split("=")[1],
                'cntntsSn': cntntsSn}
                postfields = urlencode(post_data)
                curl("https://"+hoc+".ebssw.kr/mypage/userlrn/quizResultSelect.do",postfields, cookie, True, OS, browser)
                post_data = {
                'lctreSn': params[2].split("=")[1],}
                postfields = urlencode(post_data)
                curl("https://"+hoc+".ebssw.kr/mypage/userlrn/resetQuizRspns.do",postfields, cookie, True, OS, browser)
                break
            elif i == rounds:
                answer.append(explain(quesnum,params,cnts,hoc,q,cookie)[0])
            curl("https://"+hoc+".ebssw.kr/mypage/userlrn/resetQuizRspns.do",postfields, cookie, True, OS, browser)
    return answer
